call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
 
set cores=%NUMBER_OF_PROCESSORS%
echo Building boost with %cores% cores
 
cd boost_1_65_1
call bootstrap.bat
 
rem Most libraries can be static libs
b2 -j%cores% toolset=msvc-14.1 address-model=64 architecture=x86 link=static threading=multi runtime-link=shared --build-type=minimal stage --stagedir=stage/x64 
b2 -j%cores% toolset=msvc-14.1 address-model=32 architecture=x86 link=static threading=multi runtime-link=shared --build-type=minimal stage --stagedir=stage/win32
 
pause