#include <stm32f7xx_hal.h>
#include "thor.h"


GPIOClass testPin(GPIOA, PIN_15, HIGH_SPD, NOALTERNATE);

/* Test suite of data types for TX */
char char_string[] = "yello\n";
uint8_t uint8_single = 0x33;
uint8_t uint8_array[] = { 0x43, 0x53, 0x63, 0x73 };

uint8_t readArr[ThorDef::UART::UART_BUFFER_SIZE];


bool testTransmit = false;
bool testReceive = true;


int main(void)
{	
	HAL_Init();
	ThorSystemClockConfig();

	testPin.mode(OUTPUT_PP);

	uart7->begin(9600);
	uart7->setTxModeBlock();
	uart7->setRxModeIT();
	
	memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
	uint8_t readLen = 5;
	
	for (;;)
	{

		if (testTransmit)
		{
			HAL_Delay(10);
			uart7->setTxModeBlock(); HAL_Delay(1);
			uart7->write(uint8_single);
			uart7->write(uint8_array, 4);
			uart7->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

			HAL_Delay(10);
			uart7->setTxModeIT(); HAL_Delay(1);
			uart7->write(uint8_single);
			uart7->write(uint8_array, 4);
			uart7->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

			HAL_Delay(10);
			uart7->setTxModeDMA(); HAL_Delay(1);
			uart7->write(uint8_single);
			uart7->write(uint8_array, 4);
			uart7->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));
		}

		if (testReceive)
		{
			if (uart7->availablePackets() > 0)
			{
				size_t pktSize = uart7->nextPacketSize();

				uart7->readPacket(readArr, ThorDef::UART::UART_BUFFER_SIZE);

				uart7->write(readArr, pktSize);
				HAL_Delay(5);
				memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
			}
		}
		//testPin.toggle();
		//HAL_Delay(100);
	}
}
