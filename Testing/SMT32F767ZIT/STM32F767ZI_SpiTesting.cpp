#include <stm32f7xx_hal.h>
#include <stm32_hal_legacy.h>
#include "include/thor.h"

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "include/spi.h"

using namespace ThorDef::SPI;

uint32_t dataArrIn[] = { 0x13233343, 0x53637383, 0x93031323, 0x33435363 };
uint32_t dataArrOut[] = { 0x00000000, 0x00000000, 0x00000000, 0x00000000 };

int main(void)
{
	HAL_Init();
	ThorSystemClockConfig();

	boost::shared_ptr<GPIOClass> SSPin = boost::make_shared<GPIOClass>(GPIOF, PIN_14, ULTRA_SPD, NOALTERNATE);
	SPIClass_sPtr spi = spi1;
	
	spi->attachPin(SSPin);
	spi->begin(EXTERNAL_SLAVE_SELECT);

	bool testTransmit = false;
	bool testTransmitReceive = true;
	bool testReceive = false;

	for (;;)
	{
		if (testTransmit)
		{
			spi->setTxModeBlock(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);

			spi->setTxModeInterrupt(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);

			spi->setTxModeDMA(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);
		}

		if (testTransmitReceive)
		{
			spi->setTxModeBlock(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
			HAL_Delay(5);
			memset(dataArrOut, 0, 16);

			spi->setTxModeInterrupt(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
			HAL_Delay(5);
			memset(dataArrOut, 0, 16);

	 		spi->setTxRxModeDMA(); HAL_Delay(1);
	 		spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
	 		HAL_Delay(5);
	 		memset(dataArrOut, 0, 16);
		}
		
		if (testReceive)
		{
			/* Need slave mode for full functionality here. Otherwise
			 * use the transmit receive option to get data. */
		}
	}
}
