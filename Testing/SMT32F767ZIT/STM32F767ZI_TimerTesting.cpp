#include <stm32f7xx_hal.h>
#include "thor.h"
#include "pwm.h"


int main(void)
{
	HAL_Init();
	SystemClockConfig();


	GPIOClass LedPin(GPIOB, PIN_7, HIGH_SPD, NOALTERNATE);
	LedPin.mode(OUTPUT_PP, NOPULL);

	PWMClass PWM1(TIMER3, 1, 1);
	PWMClass PWM2(TIMER3, 2, 1);
	PWMClass PWM3(TIMER3, 3, 1);
	PWMClass PWM4(TIMER3, 4, 1);

	/* Configure and start */
	PWM1.begin(60, 0.8, (10.0 * SI_Units.MICRO));
	PWM1.start();
	
	PWM2.begin(0.4);
	PWM2.start();

	PWM3.begin(0.25);
	PWM3.start();
	 
	PWM4.begin(0.65);
	PWM4.start();
	
	PWM4.setDutyCycle(0.1);


	float incr = 0.05;

	for (;;)
	{
		LedPin.write(HIGH);
		HAL_Delay(100);
		LedPin.write(LOW);
		HAL_Delay(100);

		PWM4.setDutyCycleDelta(incr);
		if (PWM4.getDutyCycle() > 0.98)
		{
			incr = -0.05;
			HAL_Delay(750);
		}
			

		else if (PWM4.getDutyCycle() < 0.02)
		{
			incr = 0.05;
			HAL_Delay(750);
		}
			
	}
}