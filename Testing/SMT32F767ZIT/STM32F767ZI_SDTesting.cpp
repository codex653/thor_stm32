#include <stm32f7xx_hal.h>
#include <stm32_hal_legacy.h>
#include <SysprogsProfiler.h>
#include "thor.h"

#include "sd.h"
#include "sd_diskio.h"

using namespace ThorDef::SPI;


bool testLegacyCode = false;

static void Error_Handler(void);
static void CPU_CACHE_Enable(void);

int main(void)
{
	CPU_CACHE_Enable();

	HAL_Init();
	ThorSystemClockConfig();
	InitializeSamplingProfiler();

	/* Configure LED_GREEN and LED_RED */
	GPIOClass greenLed(GPIOB, PIN_0, ULTRA_SPD, NOALTERNATE);
	GPIOClass redLed(GPIOB, PIN_14, ULTRA_SPD, NOALTERNATE);
	GPIOClass_sPtr SD_SSPin = boost::make_shared<GPIOClass>(GPIOF, PIN_14, ULTRA_SPD, NOALTERNATE);

	greenLed.mode(OUTPUT_PP); greenLed.write(LOW);
	redLed.mode(OUTPUT_PP); redLed.write(LOW);

	/* SPI Setup Steps */
	spi1->attachPin(SD_SSPin);
	spi1->setSSMode(SS_MANUAL_CONTROL);

	spi1->begin(EXTERNAL_SLAVE_SELECT);
	spi1->setTxModeBlock();
	spi1->setRxModeBlock();

	
	uint8_t testTextTX[] = "\nHello foolish rebels.\n";
	size_t txSize = sizeof(testTextTX) / sizeof(testTextTX[0]);
	uint8_t testTextRX[50];
	uint32_t total = 0;
	volatile FRESULT error = FR_OK;
	memset(testTextRX, 0, 50);

	SDCard SD(spi1);
	SD.initialize();

	error = SD.fopen("STM32_2.TXT", FA_OPEN_APPEND | FA_WRITE);
	error = SD.write(testTextTX, txSize, total);
	error = SD.fclose();

	error = SD.fopen("STM32_2.TXT", FA_READ);
	error = SD.read(testTextRX, txSize, total);
	error = SD.fclose();


	if (error != FR_OK)
		while(1)
		{
			;
		}

	/* If the above doesn't work, neither will this: */
	volatile double testData[] = { 0.23434, -0.7373, 3.912345 };
	volatile size_t sizeTest = sizeof(testData);

	SD.fopen("raw_data", FA_CREATE_NEW | FA_WRITE);
	SD.write((uint8_t*)testData, sizeTest, total);
	SD.fclose();

	memset((void*)testData, 0, sizeTest);

	SD.fopen("raw_data", FA_OPEN_EXISTING | FA_READ);
	SD.read((uint8_t*)testData, sizeTest, total);
	SD.fclose();

	SD.deInitialize();

	/* Run the original example code the SDCard class was built on. Useful for checking 
	   if the problem lies with FatFS or your own code. */
	if (testLegacyCode)
	{
		FATFS SDFatFs;  /* File system object for SD disk logical drive */
		FIL MyFile;     /* File object */
		char SDPath[4]; /* SD disk logical drive path */
		uint8_t workBuffer[_MAX_SS];

		FRESULT res;												/* FatFs function common result code */
		uint32_t byteswritten, bytesread;							/* File write/read counts */
		uint8_t wtext[] = "Hey Josh!!! I just wrote an SD card!!";	/* File write buffer */
		uint8_t rtext[100];											/* File read buffer */

		if (FATFS_LinkDriver(&SD_Driver, SDPath) == 0)
		{
			/*##-2- Register the file system object to the FatFs module ##############*/
			if (f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
			{
				/* FatFs Initialization Error */
				redLed.write(HIGH);
				Error_Handler();
			}
			else
			{
				/*##-3- Create a FAT file system (format) on the logical drive #########*/
				if (f_mkfs((TCHAR const*)SDPath, FM_FAT32, 0, workBuffer, sizeof(workBuffer)) != FR_OK)
				{
					redLed.write(HIGH);
					Error_Handler();
				}
				else
				{
					/*##-4- Create and Open a new text file object with write access #####*/
					if (f_open(&MyFile, "STM32.TXT", FA_CREATE_ALWAYS | FA_WRITE) != FR_OK)
					{
						/* 'STM32.TXT' file Open for write Error */
						redLed.write(HIGH);
						Error_Handler();
					}
					else
					{
						/*##-5- Write data to the text file ################################*/
						res = f_write(&MyFile, wtext, sizeof(wtext), (UINT *)&byteswritten);

						if ((byteswritten == 0) || (res != FR_OK))
						{
							/* 'STM32.TXT' file Write or EOF Error */
							redLed.write(HIGH);
							Error_Handler();
						}
						else
						{
							/*##-6- Close the open text file #################################*/
							f_close(&MyFile);

							/*##-7- Open the text file object with read access ###############*/
							if (f_open(&MyFile, "STM32.TXT", FA_READ) != FR_OK)
							{
								/* 'STM32.TXT' file Open for read Error */
								redLed.write(HIGH);
								Error_Handler();
							}
							else
							{
								/*##-8- Read data from the text file ###########################*/
								res = f_read(&MyFile, rtext, sizeof(rtext), (UINT*)&bytesread);

								if ((bytesread == 0) || (res != FR_OK)) /* EOF or Error */
								{
									/* 'STM32.TXT' file Read or EOF Error */
									redLed.write(HIGH);
									Error_Handler();
								}
								else
								{
									/*##-9- Close the open text file #############################*/
									f_close(&MyFile);

									/*##-10- Compare read data with the expected data ############*/
									if ((bytesread != byteswritten))
									{
										/* Read data is different from the expected data */
										redLed.write(HIGH);
										Error_Handler();
									}
									else
									{
										/* Success of the demo: no error occurrence */
										greenLed.write(HIGH);
									}
								}
							}
						}
					}
				}
			}
		}
		FATFS_UnLinkDriver(SDPath);
	}
	
	for (;;)
	{
		greenLed.write(HIGH);
		HAL_Delay(250);
		greenLed.write(LOW);
		HAL_Delay(250);
	}

	
}

static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

static void Error_Handler(void)
{
	while (1)
	{
	}
}