#include <stm32f7xx_hal.h>
#include "gpio.h"
#include "definitions.h"
#include "stm32f7xx_ll_gpio.h"

#ifdef __cplusplus
extern "C"
#endif
void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
}

int main(void)
{
	HAL_Init();
	
	/*

	__GPIOE_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = GPIO_PIN_8;

	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	*/
	
	//LL_GPIO_SetPinPull(GPIOE, LL_GPIO_PIN_15, LL_GPIO_PULL_UP);
	
	GPIOClass testPin(GPIOE, PIN_8, HIGH_SPD, NO_ALTERNATE);
	
	testPin.mode(INPUT, PULLDN);
	
	for (;;)
	{
		//HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_SET);
		HAL_Delay(5);
		//HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET);
		HAL_Delay(5);
	}
}

//GPIOA, 15