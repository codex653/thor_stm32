#include <stm32f4xx_hal.h>
#include <stm32_hal_legacy.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "../../include/thor.h"
#include "../../include/spi.h"

#include "stm32f7_lsm9ds0.h"

using namespace ThorDef::SPI;
using namespace ThorDef::GPIO;

uint32_t dataArrIn[] = { 0x13233343, 0x53637383, 0x93031323, 0x33435363 };
uint32_t dataArrOut[] = { 0x00000000, 0x00000000, 0x00000000, 0x00000000 };

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#include "STM32F446RE_Threaded_SPITesting.h"

void ledStatus(void* argument);
#endif 

int main(void)
{
	HAL_Init();
	ThorInit();

	#ifdef USING_FREERTOS
	xTaskCreate(ledStatus, "ledStat", 500, NULL, 1, NULL);
	xTaskCreate(readSensor, "sensor", 5000, NULL, 3, NULL);
	xTaskCreate(writeSD, "sd", 5000, NULL, 1, NULL);
	
	vTaskStartScheduler();
	#endif
	
	#ifndef USING_FREERTOS
	GPIOClass_sPtr SSPin = boost::make_shared<GPIOClass>(GPIOB, PIN_12, ULTRA_SPD, NOALTERNATE);
	SPIClass_sPtr spi = spi2;
	
	spi->attachPin(SSPin);
	spi->begin(EXTERNAL_SLAVE_SELECT);
	
	
	
	LSM9DS0_Settings sensor_settings;

	/* Fill in the Accelerometer and Mag details */ 
	sensor_settings.scale.accel = A_SCALE_2G;
	sensor_settings.scale.mag = M_SCALE_2GS;
	sensor_settings.scale.gyro = G_SCALE_245DPS;
				   
	sensor_settings.odr.accel = A_ODR_50;
	sensor_settings.odr.mag = M_ODR_50;
	sensor_settings.odr.gyro = G_ODR_190_BW_125;
	

	/* Create the new sensor object */
	SPIClass_sPtr lsm_spi = spi3;
	GPIOClass_sPtr lsm_ss_xm = boost::make_shared<GPIOClass>(GPIOA, PIN_9, ULTRA_SPD, NOALTERNATE);
	GPIOClass_sPtr lsm_ss_g = boost::make_shared<GPIOClass>(GPIOA, PIN_8, ULTRA_SPD, NOALTERNATE);
	
	//lsm_ss_g->mode(OUTPUT_PP); lsm_ss_g->write(HIGH);
	//lsm_ss_xm->mode(OUTPUT_PP); lsm_ss_xm->write(HIGH);
	
	LSM9DS0 sensor(lsm_spi, lsm_ss_xm, lsm_ss_g, sensor_settings);
	sensor.initialize();
	
	bool testTransmit = false;
	bool testTransmitReceive = false;
	bool testReceive = false;

	
	volatile float ax = 0.0, ay = 0.0, az = 0.0;
	uint8_t test[] = { 0x33, 0x44, 0x55 };
	
	for (;;)
	{
		sensor.readAll();
		sensor.calcAll();
		
		ax = sensor.sensorData.accel.x;
		ay = sensor.sensorData.accel.y;
		az = sensor.sensorData.accel.z;
		
 		HAL_Delay(1);
		
		if (testTransmit)
		{
			spi->setTxModeBlock(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);

			spi->setTxModeInterrupt(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);

			spi->setTxModeDMA(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, 16);
			HAL_Delay(5);
		}

		if (testTransmitReceive)
		{
			spi->setTxModeBlock(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
			HAL_Delay(5);
			memset(dataArrOut, 0, 16);

			spi->setTxModeInterrupt(); HAL_Delay(1);
			spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
			HAL_Delay(5);
			memset(dataArrOut, 0, 16);

	 		spi->setTxRxModeDMA(); HAL_Delay(1);
	 		spi->write((uint8_t*)dataArrIn, (uint8_t*)dataArrOut, 16);
	 		HAL_Delay(5);
	 		memset(dataArrOut, 0, 16);
		}
		
		if (testReceive)
		{
			/* Need slave mode for full functionality here. Otherwise
			 * use the transmit receive option to get data. */
		}
	}
	#endif
}

#ifdef USING_FREERTOS
void ledStatus(void *argument)
{
	GPIOClass_sPtr ledPin = boost::make_shared<GPIOClass>(GPIOA, PIN_5, ULTRA_SPD, NOALTERNATE);
	ledPin->mode(OUTPUT_PP);
	ledPin->write(LOW);
	
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for (;;)
	{
		ledPin->write(HIGH);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
		ledPin->write(LOW);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
	}
}
#endif