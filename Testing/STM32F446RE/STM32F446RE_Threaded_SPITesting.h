#pragma once
#ifndef THREADED_SPI_TESTING_H_
#define THREADED_SPI_TESTING_H_
#include <stdint.h>

#include "../../include/thor_config.h"
#include "../../include/thor.h"
#include "../../include/thor_definitions.h"
#include "../../include/interrupt.h"
#include "../../include/spi.h"
#include "../../include/exti.h"
#include "stm32f7_lsm9ds0.h"

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif 

extern void readSensor(void* argument);
extern void writeSD(void* argument);

#endif