#include <stm32f4xx_hal.h>
#include <stm32_hal_legacy.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "../../include/thor.h"
#include "../../libraries/ESC/esc_quad.hpp"
#include "STM32F446RE_PWMTesting_ESC.h"


using namespace ThorDef::SPI;
using namespace ThorDef::GPIO;

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"

void ledStatus(void* argument);
#endif 

int main(void)
{
	HAL_Init();
	ThorInit();

	#ifdef USING_FREERTOS
	GPIOClass_sPtr pin = boost::make_shared<GPIOClass>(GPIOC, PIN_7, ULTRA_SPD, NOALTERNATE);
	
	xTaskCreate(esc_test, "esc", 2000, NULL, 1, NULL);
	xTaskCreate(ledStatus, "basicTask", 250, NULL, 1, NULL);
	vTaskStartScheduler();
	#endif
	
	#ifndef USING_FREERTOS
	
	
	for (;;)
	{
		
	}
	#endif
}

#ifdef USING_FREERTOS
void ledStatus(void *argument)
{
	GPIOClass_sPtr ledPin = boost::make_shared<GPIOClass>(GPIOA, PIN_5, ULTRA_SPD, NOALTERNATE);
	ledPin->mode(OUTPUT_PP);
	ledPin->write(LOW);
	
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for (;;)
	{
		ledPin->write(HIGH);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
		ledPin->write(LOW);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
	}
}
#endif