#include "uart_threaded_test.h"
#include "../../include/exti.h"

using namespace ThorDef::GPIO;
using namespace Interrupt;
using namespace Libraries::Serial;

/* Test suite of data types for TX */
char char_string[] = "yello\n";
uint8_t uint8_single = 0x33;
uint8_t uint8_array[] = { 0x43, 0x53, 0x63, 0x73 };


void uart_test_transmit(void* argument)
{
	/* Initialize the uart */
	uart4->begin(115200);
	uart4->setTxModeBlock();
	uart4->setRxModeBlock();
	
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for (;;)
	{
// 		vTaskDelay(pdMS_TO_TICKS(10));
// 		uart4->setTxModeBlock();
// 		uart4->write(&uint8_single, 1);
// 		uart4->write(uint8_array, 4);
// 		uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

		vTaskDelay(pdMS_TO_TICKS(10));
		uart4->setTxModeIT();
		uart4->write(&uint8_single, 1);
		uart4->write(uint8_array, 4);
		uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

// 		vTaskDelay(pdMS_TO_TICKS(10));
// 		uart4->setTxModeDMA();
// 		uart4->write(&uint8_single, 1);
// 		uart4->write(uint8_array, 4);
// 		uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));
	}
	
}

void uart_test_receive(void* argument)
{
	/* Initialize the uart */
	uart4->begin(115200);
	uart4->setTxModeDMA();
	uart4->setRxModeDMA();

	uint8_t readArr[ThorDef::UART::UART_BUFFER_SIZE];
	memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
	
	/* Initialize the semaphore */
	SemaphoreHandle_t dataReadySem = xSemaphoreCreateCounting(ThorDef::UART::UART_PACKET_QUEUE_SIZE, 0);
	EXTI0_TaskMGR->logEventConsumer(SRC_UART, 4, &dataReadySem);

	
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for (;;)
	{
		/* Check to see if ANY data is ready */
		BaseType_t takeResult = xSemaphoreTake(dataReadySem, 0);
		
		if (takeResult != pdPASS)
		{
			vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(10));
		}
		else if(uart4->availablePackets() > 0)
		{
			memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
			size_t pktSize = uart4->nextPacketSize();

			uart4->readPacket(readArr, ThorDef::UART::UART_BUFFER_SIZE);

			uart4->write(readArr, pktSize);
		}
	}

}