#pragma once
#ifndef UART_THREADED_TEST_H_
#define UART_THREADED_TEST_H_

#include <stdint.h>

#include "../../include/thor_config.h"
#include "../../include/thor.h"
#include "../../include/thor_definitions.h"
#include "../../include/interrupt.h"
#include "../../include/uart.h"

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif 

void uart_test_transmit(void* argument);
void uart_test_receive(void* argument);

#endif