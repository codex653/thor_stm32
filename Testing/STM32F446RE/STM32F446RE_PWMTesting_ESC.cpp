#include "STM32F446RE_PWMTesting_ESC.h"
#include <math.h>

void esc_test(void* argument)
{
	GPIOClass_sPtr m1 = boost::make_shared<GPIOClass>(GPIOB, PIN_6, ULTRA_SPD, GPIO_AF2_TIM4);
	GPIOClass_sPtr m2 = boost::make_shared<GPIOClass>(GPIOB, PIN_7, ULTRA_SPD, GPIO_AF2_TIM4);
	GPIOClass_sPtr m3 = boost::make_shared<GPIOClass>(GPIOB, PIN_8, ULTRA_SPD, GPIO_AF2_TIM4);
	GPIOClass_sPtr m4 = boost::make_shared<GPIOClass>(GPIOB, PIN_9, ULTRA_SPD, GPIO_AF2_TIM4);
	
	ESCQuad esc(4, m1, m2, m3, m4);
	
	esc.initialize(1060, 1860);
	esc.enable();
	
	TickType_t lastTimeWoken = xTaskGetTickCount();
	vTaskDelayUntil(&lastTimeWoken, pdMS_TO_TICKS(4000));
	
	esc.sendCMD(1200);
	
	uint16_t throttle_cmd = 1200;
	float x = 0.0;
	
	for (;;)
	{
		
		throttle_cmd = (uint16_t)(300*(sin(x) + 1) + 1200);
		x += 0.9;
		x = fmod(x, (2.0f * 3.14159f));
		
		esc.sendCMD(throttle_cmd);
		
		vTaskDelayUntil(&lastTimeWoken, pdMS_TO_TICKS(100));
	}
}