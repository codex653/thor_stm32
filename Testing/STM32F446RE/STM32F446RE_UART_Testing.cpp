#include <stm32f4xx_hal.h>
#include <stm32_hal_legacy.h>

#include "SysprogsProfiler.h"
#include "../../include/thor_config.h"
#include "../../include/thor.h"
#include "../../include/uart.h"
#include "../../include/gpio.h"

using namespace ThorDef::GPIO;
using namespace ThorDef::UART;

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#include "uart_threaded_test.h"

void ledStatus(void* argument);
#endif 


bool testTransmit = true;
bool testReceive = false;


int main(void)
{
	HAL_Init();
	ThorInit();
	
	InitializeSamplingProfiler();
	
	/* Test suite of data types for TX */
	char char_string[] = "yello\n";
	uint8_t uint8_single = 0x33;
	uint8_t uint8_array[] = { 0x43, 0x53, 0x63, 0x73 };

	uint8_t readArr[ThorDef::UART::UART_BUFFER_SIZE];
	
	#ifdef USING_FREERTOS
	//xTaskCreate(uart_test_transmit, "tx_thread", 1000, NULL, 2, NULL);
	xTaskCreate(uart_test_receive, "rx_thread", 5000, NULL, 1, NULL);

	xTaskCreate(ledStatus, "ledStat", 500, NULL, 1, NULL);
	
	/* Main thread should exit here and pass control over to the scheduler */
	vTaskStartScheduler();
	
	#else	uart4->begin(115200);
	uart4->setTxModeBlock();
	uart4->setRxModeBlock();

	memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
	#endif
	
	
	
	/* Should never get here if compiling with FreeRTOS */
	for (;;)
	{

		if (testTransmit)
		{
			HAL_Delay(10);
			uart4->setTxModeBlock(); HAL_Delay(1);
			uart4->write(&uint8_single, 1);
			uart4->write(uint8_array, 4);
			uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

			HAL_Delay(10);
			uart4->setTxModeIT(); HAL_Delay(1);
			uart4->write(&uint8_single, 1);
			uart4->write(uint8_array, 4);
			uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));

			HAL_Delay(10);
			uart4->setTxModeDMA(); HAL_Delay(1);
			uart4->write(&uint8_single, 1);
			uart4->write(uint8_array, 4);
			uart4->write(char_string, (size_t)(sizeof(char_string) / sizeof(char_string[0])));
		}

		if (testReceive)
		{
			if (uart4->availablePackets() > 0)
			{
				size_t pktSize = uart4->nextPacketSize();

				uart4->readPacket(readArr, ThorDef::UART::UART_BUFFER_SIZE);

				uart4->write(readArr, pktSize);
				HAL_Delay(5);
				memset(readArr, 0, ThorDef::UART::UART_BUFFER_SIZE);
			}
		}
	}
}

#ifdef USING_FREERTOS
void ledStatus(void *argument)
{
	GPIOClass_sPtr ledPin = boost::make_shared<GPIOClass>(GPIOA, PIN_5, ULTRA_SPD, NOALTERNATE);
	ledPin->mode(OUTPUT_PP);
	ledPin->write(LOW);
	
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for (;;)
	{
		ledPin->write(HIGH);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
		ledPin->write(LOW);
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(500));
	}
}
#endif