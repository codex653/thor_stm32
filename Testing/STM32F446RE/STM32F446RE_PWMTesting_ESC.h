#pragma once
#ifndef ESC_THREADED_TEST_H_
#define ESC_THREADED_TEST_H_

#include <stdint.h>

#include "../../include/thor_config.h"
#include "../../include/thor.h"
#include "../../include/thor_definitions.h"
#include "../../include/interrupt.h"
#include "../../include/uart.h"
#include "../../libraries/ESC/esc_quad.hpp"

#ifdef USING_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif 

extern void esc_test(void* argument);

#endif