#include "STM32F446RE_Threaded_SPITesting.h"



void readSensor(void* argument)
{
	LSM9DS0_Settings sensor_settings;

	/* Fill in the Accelerometer and Mag details */ 
	sensor_settings.scale.accel = A_SCALE_2G;
	sensor_settings.scale.mag = M_SCALE_2GS;
	sensor_settings.scale.gyro = G_SCALE_245DPS;
				   
	sensor_settings.odr.accel = A_ODR_50;
	sensor_settings.odr.mag = M_ODR_50;
	sensor_settings.odr.gyro = G_ODR_190_BW_125;
	
	/* Create the new sensor object */
	SPIClass_sPtr lsm_spi = spi3;
	GPIOClass_sPtr lsm_ss_xm = boost::make_shared<GPIOClass>(GPIOA, PIN_9, ULTRA_SPD, NOALTERNATE);
	GPIOClass_sPtr lsm_ss_g = boost::make_shared<GPIOClass>(GPIOA, PIN_8, ULTRA_SPD, NOALTERNATE);
	
	LSM9DS0 sensor(lsm_spi, lsm_ss_xm, lsm_ss_g, sensor_settings);
	sensor.initialize(); 
	
	TickType_t xPreviousWakeTime = xTaskGetTickCount();
	for (;;)
	{
		sensor.readAll();
		sensor.calcAll();
		
		vTaskDelayUntil(&xPreviousWakeTime, pdMS_TO_TICKS(10));
	}
}

void writeSD(void* argument)
{
	//nothing here yet
	
	TickType_t xPreviousWakeTime = xTaskGetTickCount();
	for (;;)
	{
		vTaskDelayUntil(&xPreviousWakeTime, pdMS_TO_TICKS(100));
	}
}